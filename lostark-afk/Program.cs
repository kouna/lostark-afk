﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace lostark_afk {
    class Program {
        static void Main(string[] args) {
            AfkMacro();
            while (true) {
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string strClassName, string strWindowName);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, ref Rect rectangle);

        [DllImport("user32.dll")]
        public static extern bool ClientToScreen(IntPtr hWnd, ref Point point);

        [DllImport("user32.dll")]
        public static extern long SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern IntPtr SetActiveWindow(IntPtr hWnd);


        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        internal static extern uint SendInput(uint nInputs, [MarshalAs(UnmanagedType.LPArray), In] Input[] pInputs, int cbSize);

        internal struct Input {
            public UInt32 Type;
            public MouseKeyBDHardwareInput Data;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct MouseKeyBDHardwareInput {
            [FieldOffset(0)]
            public MouseInput Mouse;
        }

        internal struct MouseInput {
            public Int32 X;
            public Int32 Y;
            public UInt32 MouseData;
            public UInt32 Flags;
            public UInt32 Time;
            public IntPtr ExtraInfo;
        }

        public struct Rect {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }

            public int Width { get { return Right - Left; } }

            public int Height { get { return Bottom - Top; } }

            public override string ToString() {
                return $"L:{Left} T:{Top} R:{Right} B:{Bottom} W:{Width} H:{Height}";
            }
        }

        static int OFFSET_LEFT = 3;
        static int OFFSET_TOP = 26;
        static int OFFSET_RIGHT = -3;
        static int OFFSET_BOTTOM = -3;

        public static Rect WindowContent(ref Rect source) {
            var content = new Rect();

            content.Left = source.Left + OFFSET_LEFT;
            content.Top = source.Top + OFFSET_TOP;
            content.Right = source.Right + OFFSET_RIGHT;
            content.Bottom = source.Bottom + OFFSET_BOTTOM;

            return content;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Point {
            public int x;
            public int y;

            public Point(int x, int y) {
                this.x = x;
                this.y = y;
            }

            public override string ToString() {
                return $"X: {x} Y: {y}";
            }
        }

        static readonly int STEP_COUNT = 64;
        static readonly int RADIUS = 128;
        static readonly int START_DELAY = 1000;
        static readonly int STEP_DELAY = 10;
        static readonly string PROCESS_NAME = "LOSTARK";

        static readonly Input INPUT_MOUSE_DOWN = new Input() { Type = 0, Data = { Mouse = { Flags = 0x0002 } } };
        static readonly Input INPUT_MOUSE_UP = new Input() { Type = 0, Data = { Mouse = { Flags = 0x0004 } } };

        static CancellationTokenSource AfkMacro() {
            var cancelToken = new CancellationTokenSource();
            var task = Task.Factory.StartNew(async () => {

                var rect = new Rect();
                bool prev = false;
                while (!cancelToken.IsCancellationRequested) {
                    var process = Process.GetProcessesByName(PROCESS_NAME)[0];
                    IntPtr ptr = process.MainWindowHandle;
                    if (GetForegroundWindow() == ptr) {
                        if (!prev) {
                            Console.WriteLine($"active window found, starting in {START_DELAY}ms");
                            prev = true;
                            await Task.Delay(START_DELAY);
                        }
                        GetWindowRect(ptr, ref rect);

                        var win = WindowContent(ref rect);
                        var center = new Point(
                            Convert.ToInt32(win.Left + win.Width / 2),
                            Convert.ToInt32(win.Top + win.Height / 2));

                        Console.WriteLine($"center:({center}) window:({rect}) content:({win})");

                        for (int step = 0; step <= STEP_COUNT; step++) {
                            SendInput(1, new Input[] { INPUT_MOUSE_DOWN }, Marshal.SizeOf(typeof(Input)));

                            double progress = Convert.ToDouble(step) / Convert.ToDouble(STEP_COUNT);
                            double rads = Math.PI * 2 * progress;
                            int x = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(RADIUS * Math.Cos(rads))));
                            int y = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(RADIUS * Math.Sin(rads))));
                            SetCursorPos(center.x + x, center.y + y);
                            await Task.Delay(STEP_DELAY);

                            SendInput(1, new Input[] { INPUT_MOUSE_UP }, Marshal.SizeOf(typeof(Input)));
                        }
                    } else {
                        prev = true;
                        Console.WriteLine("waiting for active process");
                        await Task.Delay(START_DELAY);
                    }
                }

            }, cancelToken.Token);

            return cancelToken;
        }
    }
}
